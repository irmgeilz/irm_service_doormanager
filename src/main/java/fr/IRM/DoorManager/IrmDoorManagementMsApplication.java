package fr.IRM.DoorManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IrmDoorManagementMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(IrmDoorManagementMsApplication.class, args);
	}

}
